import Vue from 'vue'
import VueSemanticUi from '../../vuejs-semantic-ui'
import router from './router'
import store from './store'
import App from './App'
import Sidebar from './components/Sidebar'

Vue.use(VueSemanticUi)

Vue.config.productionTip = false

Vue.mixin({
  data() {
    return {
      $demoList: [],
      $tabs: []
    }
  },

  created() {
    this.$demoList = [
      {
        title: 'Elements',
        list: [
          { title: 'Button' },
          {
            title: 'Container',
            list: [
              'Definition',
              'Examples'
            ]
          },
          { title: 'Divider' },
          { title: 'Header' },
          { title: 'Icon' },
          { title: 'Image' },
          { title: 'Input' },
          { title: 'Label' },
          { title: 'List' },
          { title: 'Loader' },
          { title: 'Rail' },
          { title: 'Reveal' },
          { title: 'Segment' },
        ]
      },

      {
        title: 'Collections',
        list: [
          { title: 'Breadcrumb' },
          { title: 'Form' },
          { title: 'Grid' },
          { title: 'Message' },
          { title: 'Menu' },
          { title: 'Table' },
        ]
      },

      {
        title: 'Views',
        list: [
          { title: 'Card' }
        ]
      },

      {
        title: 'Modules',
        list: [
          { title: 'Accordion' },
          { title: 'Checkbox' },
          { title: 'Dimmer' },
          { title: 'Dropdown' },
          { title: 'Tab' },
        ]
      }
    ];

    this.$tabs = [
      'Definition',
      'Examples',
      'Usage',
      'Settings'
    ]
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})

new Vue({
  el: '#sidebar',
  router,
  store,
  components: { Sidebar },
  template: '<Sidebar />'
})
