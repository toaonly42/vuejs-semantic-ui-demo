import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '',
      redirect: '/Demo'
    },
    {
      path: '/Demo',
      component: () => import('../components/Demo'),
      children: [
        {
          path: 'Button',
          component: () => import('../components/Demo/button')
        },
        {
          path: 'Container',
          redirect: {
            path: '/Demo/Container/Definition',
            query: {
              list: [ 'Definition', 'Examples' ]
            }
          }
        },
        {
          path: 'Container/:tab',
          name: 'ContainerTab',
          component: () => import('../components/Demo/container')
        },
        {
          path: 'Label',
          component: () => import('../components/Demo/label')
        },
        {
          path: 'Image',
          component: () => import('../components/Demo/image')
        },
        {
          path: 'Segment',
          component: () => import('../components/Demo/segment')
        },
        {
          path: 'Divider',
          component: () => import('../components/Demo/divider')
        },
        {
          path: 'Header',
          component: () => import('../components/Demo/header')
        },
        {
          path: 'Icon',
          component: () => import('../components/Demo/icon')
        },
        {
          path: 'Input',
          component: () => import('../components/Demo/input')
        },
        {
          path: 'List',
          component: () => import('../components/Demo/list')
        },
        {
          path: 'Loader',
          component: () => import('../components/Demo/loader')
        },
        {
          path: 'Rail',
          component: () => import('../components/Demo/rail')
        },
        {
          path: 'Reveal',
          component: () => import('../components/Demo/reveal')
        },
        {
          path: 'Breadcrumb',
          component: () => import('../components/Demo/breadcrumb')
        },
        {
          path: 'Form',
          component: () => import('../components/Demo/form')
        },
        {
          path: 'Grid',
          component: () => import('../components/Demo/grid')
        },
        {
          path: 'Message',
          component: () => import('../components/Demo/message')
        },
        {
          path: 'Menu',
          component: () => import('../components/Demo/menu')
        },
        {
          path: 'Table',
          component: () => import('../components/Demo/table')
        },

        {
          path: 'Card',
          component: () => import('../components/Demo/card')
        },

        {
          path: 'Accordion',
          name: 'Accordion',
          redirect: '/Demo/Accordion/Definition'
        },
        {
          path: 'Accordion/:tab',
          name: 'AccordionTab',
          component: () => import('../components/Demo/accordion')
        },
        {
          path: 'Checkbox',
          name: 'Checkbox',
          redirect: '/Demo/Checkbox/Definition'
        },
        {
          path: 'Checkbox/:tab',
          name: 'CheckboxTab',
          component: () => import('../components/Demo/checkbox')
        },
        {
          path: 'Dimmer',
          name: 'Dimmer',
          redirect: '/Demo/Dimmer/Definition'
        },
        {
          path: 'Dimmer/:tab',
          name: 'DimmerTab',
          component: () => import('../components/Demo/dimmer')
        },
        {
          path: 'Dropdown',
          name: 'Dropdown',
          redirect: '/Demo/Dropdown/Definition'
        },
        {
          path: 'Dropdown/:tab',
          name: 'DropdownTab',
          component: () => import('../components/Demo/dropdown')
        },
        {
          path: 'Dropdown/:tab',
          name: 'DropdownTab',
          component: () => import('../components/Demo/dropdown')
        },
        {
          path: 'Tab/:tab',
          component: () => import('../components/Demo/tab')
        }
      ]
    },

    {
      path: '/MenuVariationsFixed1',
      component: () => import('../components/Demo/menu/Fixed1')
    },
    {
      path: '/MenuVariationsFixed2',
      component: () => import('../components/Demo/menu/Fixed2')
    }
  ],
  scrollBehavior: () => ({ y: 0 })
});
