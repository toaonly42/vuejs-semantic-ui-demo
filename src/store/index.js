import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    sidebar: {
      visible: false
    }
  },

  getters: {
    sidebar: state => state.sidebar
  }
})

export default store
